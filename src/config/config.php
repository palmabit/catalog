<?php

return array(
    "client_per_page" => 9,
    "admin_per_page" => 50,
    "orders_per_page" => 25,
);