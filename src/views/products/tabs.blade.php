<ul class="nav nav-tabs" id="tab-prodotto">
    <li class="active tab-link" id="link-tab-base"><a href="#tab-generale" {{$presenter->get_toggle()}} >Generale</a></li>
    <li class="tab-link" id="link-tab-categoria"><a href="#tab-categoria" {{$presenter->get_toggle()}} >Categoria</a></li>
    <li id="link-tab-immagini" class="tab-link"><a href="#tab-immagini" {{$presenter->get_toggle()}} >Immagini</a></li>
    <li id="link-tab-accessories" class="tab-link"><a href="#tab-accessories" {{$presenter->get_toggle()}} >Accessori</a></li>
</ul>
<br/>