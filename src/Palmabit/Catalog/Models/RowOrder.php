<?php  namespace Palmabit\Catalog\Models;
/**
 * Class RowOrder
 *
 * @author jacopo beschi j.beschi@palmabit.com
 */
use Illuminate\Database\Eloquent\Model;
use Palmabit\Authentication\Exceptions\LoginRequiredException;
use App, Config;
use Palmabit\Catalog\Presenters\PresenterProducts;

class RowOrder extends Model
{
    protected $table = "row_order";

    protected $fillable = ["order_id", "product_id", "quantity", "total_price", "slug_lang", "price_type_used", "single_price"];

    protected $authenticator;

    protected $group_professional;

    protected $group_logged;

    public function getAuthenticator()
    {
        return isset($this->authenticator) ? $this->authenticator : App::make('authenticator');
    }

    public function order()
    {
        return $this->belongsTo('Palmabit\Catalog\Models\Order','order_id');
    }

    public function product()
    {
        return $this->belongsTo('Palmabit\Catalog\Models\Product','product_id');
    }

    /**
     * @param Product $product
     * @param Integer $quantity
     */
    public function setItem(Product $product, $quantity)
    {
        $this->setAttribute('product_id', $product->id);
        $this->setAttribute('slug_lang', $product->slug_lang);
        $this->setAttribute('quantity', $quantity);
        $this->setAttribute('total_price', $this->calculatePrice($product, $quantity));
        $this->setAttribute('single_price', $product->price);
    }

    /**
     * @param Product $product
     * @param Integer $quantity
     */
    public function calculatePrice(Product $product, $quantity)
    {
        if(! $this->getAuthenticator()->check()) throw new LoginRequiredException;

        return $this->multiplyMoney($product->price, $quantity);
    }

    protected function multiplyMoney($price, $quantity)
    {
        return round( ($price * $quantity) , 2);
    }

    public function getProductPresenter()
    {
        return new PresenterProducts($this->product);
    }

} 